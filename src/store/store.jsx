import { configureStore } from "@reduxjs/toolkit";
import modeReducer from "./features/mode";

const store = configureStore({
  reducer: {
    mode: modeReducer,
  },
});

export default store;
