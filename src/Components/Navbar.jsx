import React, { useEffect, useState } from "react";
import { Icon } from "@iconify/react";
import { useDispatch, useSelector } from "react-redux";
import { changeMode, modeAction } from "../store/features/mode";

const Navbar = () => {
  const modes = localStorage.getItem("mode");
  const dispatch = useDispatch();
  const { mode } = useSelector((state) => state.mode);

  const modeHandler = (m) => {
    dispatch(changeMode(m));
    localStorage.setItem("mode", m);
  };

  useEffect(() => {
    dispatch(changeMode(modes));
  }, []);
  return (
    <div
      className={
        mode === "light" ? "bg-white color-black" : "bg-dark-blue color-white"
      }
    >
      <div className='container d-flex justify-content-between align-items-center py-2 px-4'>
        <h1
          className={`title ${mode === "light" ? "text-dark" : "text-white"} `}
        >
          Where in the world?
        </h1>
        {mode === "light" ? (
          <div
            className='d-flex align-items-center cursor-pointer'
            onClick={() => modeHandler("dark")}
          >
            <Icon icon='fa6-regular:moon' />
            <p
              className={`ms-1  ${
                mode === "light" ? "text-dark" : "text-white"
              } `}
            >
              {" "}
              Dark Mode
            </p>
          </div>
        ) : (
          <div
            className='d-flex align-items-center cursor-pointer'
            onClick={() => modeHandler("light")}
          >
            <Icon icon='bxs:sun' color={mode === "light" ? "black" : "white"} />
            <p
              className={`ms-1  ${
                mode === "light" ? "text-dark" : "text-white"
              } `}
            >
              {" "}
              Light Mode
            </p>
          </div>
        )}
      </div>
    </div>
  );
};

export default Navbar;
