import React, { useState } from "react";
import { Icon } from "@iconify/react";
import InputAdornment from "@mui/material/InputAdornment";
import TextField from "@mui/material/TextField";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import { useDispatch, useSelector } from "react-redux";
import { InputBase, styled } from "@mui/material";

const Search = ({ selectRegion, setSelectRegion, search, setSearch }) => {
  const { mode } = useSelector((state) => state.mode);
  const CssTextField = styled(TextField)({
    "& .MuiOutlinedInput-root": {
      color: mode === "light" ? "hsl(0, 0%, 52%)" : "white",
      "& fieldset": {
        borderColor: "transparent",
      },
      "&:hover fieldset": {
        borderColor: "transparent",
      },
      "&.Mui-focused fieldset": {
        borderColor: "transparent",
      },
    },
  });

  return (
    <div className='container px-4'>
      <div className='d-flex justify-content-between flex-wrap'>
        <div className='search shadow-card d-flex align-items-center bg-white mt-5'>
          <CssTextField
            placeholder='Search for a country...'
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            sx={{
              background: mode === "light" ? "white" : "hsl(209, 23%, 22%)",
              width: "100%",
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position='start' sx={{ px: 2 }}>
                  <Icon
                    icon='fa:search'
                    color={mode === "light" ? "hsl(209, 23%, 22%)" : "white"}
                  />
                </InputAdornment>
              ),
            }}
          />
        </div>
        <div className='dropdown bg-white shadow-card mt-5'>
          <FormControl
            sx={{
              minWidth: "100%",
              background: mode === "light" ? "white" : "hsl(209, 23%, 22%)",
              "& .MuiOutlinedInput-root": {
                "& fieldset": {
                  borderColor: "transparent",
                },
                "&:hover fieldset": {
                  borderColor: "transparent",
                },
                "&.Mui-focused fieldset": {
                  borderColor: "transparent",
                },
              },
            }}
          >
            <InputLabel
              id='demo-simple-select-helper-label'
              sx={{ color: mode === "light" ? "hsl(209, 23%, 22%)" : "white" }}
            >
              Filter by Region
            </InputLabel>
            <Select
              sx={{
                color: mode === "light" ? "hsl(209, 23%, 22%)" : "white",
                width: "100%",
                "& .MuiFormLabel-root": {
                  color: "white",
                },
              }}
              MenuProps={{
                PaperProps: {
                  sx: {
                    background:
                      mode === "light" ? "white" : " hsl(209, 23%, 22%)",
                    color: mode === "light" ? " hsl(209, 23%, 22%)" : "white",
                  },
                },
              }}
              labelId='demo-simple-select-helper-label'
              id='demo-simple-select-helper'
              inputProps={{ "aria-label": "Without label" }}
              value={selectRegion}
              onChange={(e) => setSelectRegion(e.target.value)}
            >
              <MenuItem value={""}>
                <em>All</em>
              </MenuItem>
              <MenuItem value={"Africa"}>Africa</MenuItem>
              <MenuItem value={"Americas"}>Americas</MenuItem>
              <MenuItem value={"Asia"}>Asia</MenuItem>
              <MenuItem value={"Europe"}>Europe</MenuItem>
              <MenuItem value={"Oceania"}>Oceania</MenuItem>
            </Select>
          </FormControl>
        </div>
      </div>
    </div>
  );
};

export default Search;
