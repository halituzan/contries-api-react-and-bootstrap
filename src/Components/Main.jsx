import React, { useEffect, useState } from "react";
import ReactFlipCard from "reactjs-flip-card";
import FrontCard from "./MainComponents/FrontCard";
import CountryPage from "./MainComponents/CountryPage";
import { useDispatch, useSelector } from "react-redux";
import { changeMode } from "../store/features/mode";
const Main = ({ countryList }) => {
  const [selectCountry, setSelectCountry] = useState(null);
  const { mode } = useSelector((state) => state.mode);
  const dispatch = useDispatch();
  const [dataCount, setDataCount] = useState(7);
  const modes = localStorage.getItem("mode");
  useEffect(() => {
    if (!mode) {
      dispatch(changeMode("light"));
    } else {
      dispatch(changeMode(modes));
    }
  }, [mode]);

  const selectHandle = (item) => {
    setSelectCountry(item);
  };

  return (
    <div className={`container mt-5 px-4`} style={{ flex: 1 }}>
      <div className='row g-5'>
        {!selectCountry ? (
          countryList
            ?.sort((a, b) => b.population - a.population)
            .map((item, index) => {
              if (index > dataCount) return;
              return (
                <div className={`col-12 col-sm-6 col-md-4 col-lg-3 `}>
                  <ReactFlipCard
                    containerCss='w-100 h-290'
                    flipCardCss='w-100'
                    frontStyle={{ minHeight: "300px" }}
                    frontComponent={
                      <FrontCard
                        mode={mode}
                        item={item}
                        select={() => selectHandle(item)}
                      />
                    }
                    backComponent={
                      <FrontCard
                        mode={mode}
                        item={item}
                        select={() => selectHandle(item)}
                      />
                    }
                  />
                </div>
              );
            })
        ) : (
          <CountryPage
            data={countryList}
            item={selectCountry}
            setSelectCountry={setSelectCountry}
          />
        )}
        {!selectCountry && (
          <div className='d-flex justify-content-center'>
            <button
              className={`btn px-5 fw-bold ${mode === "light" ? "btn-light" : "btn-dark bg-very-dark-blue border-0"}`}
              onClick={() => setDataCount((prevState) => prevState + 8)}
            >
              Show more
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default Main;
