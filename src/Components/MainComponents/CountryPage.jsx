import { Icon } from "@iconify/react";
import Box from "@mui/material/Box";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

const CountryPage = ({ item, setSelectCountry }) => {
  const [borders, setBorders] = useState([]);
  const [data, setData] = useState([]);
  const { mode } = useSelector((state) => state.mode);

  const getCountryList = async (borders) => {
    try {
      await fetch("/api/data.json")
        .then((res) => res.json())
        .then((res) => {
          let filtered = [];
          for (const item of borders) {
            filtered.push(res.find((i) => i.alpha3Code === item));
          }
          setBorders(filtered);

          setData(res);
        });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getCountryList(item.borders);
  }, []);
  return (
    <div className='d-flex flex-column'>
      <div className='mb-4'>
        <button
          className={`border-0 shadow-card d-flex align-items-center p-1 px-4 rounded my-4 ${
            mode === "dark" ? "bg-dark-blue" : "bg-white"
          }`}
          onClick={() => setSelectCountry(null)}
          style={{ width: "120px", cursor: "pointer" }}
        >
          <Icon
            icon='teenyicons:arrow-left-outline'
            color={mode === "dark" ? "white" : "hsl(207, 26%, 17%)"}
          />
          <p
            className={`ms-2 ${mode === "dark" ? "text-white" : ""} `}
          >
            Back
          </p>
        </button>
      </div>
      <Box className='row g-5'>
        <img
          src={item.flag}
          alt={`Flag of ${item.name}`}
          height='350'
          className='col-12 col-md-6'
        />
        <Box className={`col-12 col-md-6 ${mode === "dark" && "text-white"}`}>
          <h1 className='title fw-bold '>{item.name}</h1>
          <Box className='d-flex flex-column flex-md-row justify-content-start w-100 '>
            <Box className='d-flex justify-content-start w-100  flex-column'>
              <Box className='d-flex mt-4'>
                <p className='fw-bold'>Native Name: </p>
                <span className='ms-1'>{item.nativeName}</span>
              </Box>
              <Box className='d-flex mt-1'>
                <p className='fw-bold'>Population: </p>
                <span className='ms-1'>{item.population}</span>
              </Box>
              <Box className='d-flex mt-1'>
                <p className='fw-bold'>Region: </p>
                <span className='ms-1'>{item.region}</span>
              </Box>
              <Box className='d-flex mt-1'>
                <p className='fw-bold'>Sub Region: </p>
                <span className='ms-1'>{item.subregion}</span>
              </Box>
              <Box className='d-flex mt-1'>
                <p className='fw-bold'>Capital: </p>
                <span className='ms-1'>{item.capital}</span>
              </Box>
            </Box>
            <Box className='d-flex justify-content-start w-100 flex-column'>
              <Box className='d-flex mt-4'>
                <p className='fw-bold'>Top Level Domain: </p>
                <span className='ms-1'>{item.topLevelDomain[0] ?? ""}</span>
              </Box>
              <Box className='d-flex mt-1'>
                <p className='fw-bold'>Population: </p>
                <span className='ms-1'>{item.population}</span>
              </Box>
              <Box className='d-flex mt-1'>
                <p className='fw-bold'>Region: </p>
                <span className='ms-1'>{item.region}</span>
              </Box>
            </Box>
          </Box>
          <Box className='d-flex flex-md-row flex-column w-100 mt-5'>
            <p className='fw-bold' style={{ minWidth: "150px" }}>
              Border Countries:
            </p>
            <Box className='d-flex flex-wrap'>
              {borders?.map((i) => {
                return (
                  <Box
                    sx={{ minWidth: "100px", fontSize: "12px" }}
                    className={`shadow-card m-1 d-flex justify-content-center align-items-center px-3 py-1 ${
                      mode === "dark" ? "bg-dark-blue" : "very-light-gray"
                    }`}
                  >
                    {i?.name}
                  </Box>
                );
              })}
            </Box>
          </Box>
        </Box>
      </Box>
    </div>
  );
};

export default CountryPage;
