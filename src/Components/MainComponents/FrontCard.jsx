import Box from "@mui/material/Box";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
const FrontCard = ({ item, select }) => {
  const formatPopulation = (text) => {
    text = text.toString().split("").reverse().join("");
    var output = text.replace(/(\d{3})/g, "$1,");
    if (output.slice(-1) === ",") {
      output = output.slice(0, -1);
    }
    return output.split("").reverse().join("");
  };
  const { mode } = useSelector((state) => state.mode);

  return (
    <React.Fragment>
      <CardContent
        sx={{ p: 0 }}
        className={`shadow rounded ${
          mode === "light" ? "bg-white text-black" : "bg-dark-blue text-white"
        }`}
        onClick={() => select()}
      >
        <img
          src={item?.flags?.png}
          alt=''
          className='rounded-top'
          style={{
            objectFit: "cover",
            width: "100%",
            height: "150px",
          }}
        />
        <Box sx={{ p: 2 }}>
          <Typography sx={{ fontSize: 14, fontWeight: "bold" }} gutterBottom>
            {item.name}
          </Typography>

          <Box sx={{ mt: 2 }}>
            <Box sx={{ display: "flex" }}>
              <Typography
                sx={{ fontSize: 12, fontWeight: "bold" }}
                gutterBottom
              >
                Population:
              </Typography>
              <Typography sx={{ fontSize: 12 }} gutterBottom>
                {formatPopulation(item.population)}
              </Typography>
            </Box>
            <Box sx={{ display: "flex" }}>
              <Typography
                sx={{ fontSize: 12, fontWeight: "bold" }}
                gutterBottom
              >
                Region:
              </Typography>
              <Typography sx={{ fontSize: 12 }} gutterBottom>
                {item.region}
              </Typography>
            </Box>
            <Box sx={{ display: "flex" }}>
              <Typography
                sx={{ fontSize: 12, fontWeight: "bold" }}
                gutterBottom
              >
                Capital:
              </Typography>
              <Typography sx={{ fontSize: 12 }} gutterBottom>
                {item.capital}
              </Typography>
            </Box>
          </Box>
        </Box>
      </CardContent>
    </React.Fragment>
  );
};

export default FrontCard;
