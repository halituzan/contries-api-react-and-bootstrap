import { useEffect, useState } from "react";
import Main from "./Components/Main";
import Navbar from "./Components/Navbar";
import Search from "./Components/Search";
import { useDispatch, useSelector } from "react-redux";
import { changeMode } from "./store/features/mode";

function App() {
  const [selectRegion, setSelectRegion] = useState("");
  const [countryList, setCountryList] = useState([]);
  const [search, setSearch] = useState("");
  const { mode } = useSelector((state) => state.mode);
  const modes = localStorage.getItem("mode");
  const dispatch = useDispatch();
  useEffect(() => {
    const mode = localStorage.getItem("mode");
    if (!mode) {
      localStorage.setItem("mode", "light")
      dispatch(changeMode("light"));
    } else {
      dispatch(changeMode(modes));
    }
  }, [modes]);
  const getCountryList = async () => {
    try {
      await fetch("/api/data.json")
        .then((res) => res.json())
        .then((res) => setCountryList(res));
    } catch (error) {
      console.log(error);
    }
  };
  const getRegionCountryList = async (region) => {
    try {
      await fetch("/api/data.json")
        .then((res) => res.json())
        .then((res) => setCountryList(res.filter((i) => i.region === region)));
    } catch (error) {
      console.log(error);
    }
  };
  const getSearchCountryList = async (s) => {
    try {
      await fetch("/api/data.json")
        .then((res) => res.json())
        .then((res) =>
          setCountryList(
            res.filter((i) => i.name.toLowerCase().includes(s.toLowerCase()))
          )
        );
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getCountryList();
  }, []);
  useEffect(() => {
    if (search) {
      getSearchCountryList(search);
    } else {
      getCountryList();
    }
  }, [search]);
  useEffect(() => {
    if (selectRegion) {
      getRegionCountryList(selectRegion);
    } else {
      getCountryList();
    }
  }, [selectRegion]);

  return (
    <div className={`d-flex flex-column ${mode === "light" ? "very-light-gray color-black" : "bg-very-dark-blue color-white"
      }`} style={{ minHeight: "100vh" }}>
      <Navbar />
      <Search
        setSelectRegion={setSelectRegion}
        selectRegion={selectRegion}
        search={search}
        setSearch={setSearch}
      />
      <Main countryList={countryList} />
    </div>
  );
}

export default App;
